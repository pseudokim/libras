package me.evdokim.libras.services

import javax.inject.Inject

import me.evdokim.libras.models.{Transfer, TransferExecutionError, TransferStatus}
import me.evdokim.libras.repositories.{AccountRepository, TransferRepository}

trait TransferExecutionService {
  def execute(transfer: Transfer): Transfer
}

class TransferExecutionServiceImpl @Inject() (accountRepository: AccountRepository, transferRepository: TransferRepository) extends TransferExecutionService {
  override def execute(transfer: Transfer): Transfer = {

    def updateFrom(): Either[TransferExecutionError, Unit] = {
      for {
        fromAccount <- accountRepository.getById(transfer.from) match {
          case None => Left(TransferExecutionError.AccountDoesNotExists(transfer, transfer.from))
          case Some(account) => Right(account)
        }
        newBalance = fromAccount.balance - transfer.amount
        _ <- if (newBalance < 0) {
          Left(TransferExecutionError.InsufficientBalance(transfer))
        } else {
          Right(())
        }
        newFromAccount = fromAccount.copy(balance = newBalance)
        res <- accountRepository.compareAndSet(fromAccount, newFromAccount) match {
          case false => updateFrom()
          case true => Right(())
        }
      } yield ()
    }

    def updateTo(): Either[TransferExecutionError, Unit] = {
      for {
        toAccount <- accountRepository.getById(transfer.to) match {
          case None => Left(TransferExecutionError.AccountDoesNotExists(transfer, transfer.to))
          case Some(account) => Right(account)
        }
        newToAccount = toAccount.copy(balance = toAccount.balance + transfer.amount)
        res <- accountRepository.compareAndSet(toAccount, newToAccount) match {
          case false => updateTo()
          case true => Right(())
        }
      } yield ()
    }

    def updateStatus(newStatus: TransferStatus): Transfer = {
      val newTransferValue = transfer.copy(status = newStatus)
      transferRepository.compareAndSet(transfer, transfer.copy(status = newStatus)) match {
        case false => updateStatus(newStatus)
        case true => newTransferValue
      }
    }

    val newStatus = updateFrom flatMap { _ => updateTo()} match {
      case Left(e) => TransferStatus.Declined(e)
      case Right(_) => TransferStatus.Completed
    }

    updateStatus(newStatus)
  }
}