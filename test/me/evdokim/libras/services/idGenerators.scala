package me.evdokim.libras.services

import me.evdokim.libras.models.{AccountId, TransferId}


class ConstantIdGeneratorService extends IdGeneratorService {
  val accountId: AccountId = AccountId.random()
  val transferId: TransferId = TransferId.random()

  override def generateAccountId(): AccountId = accountId
  override def generateTransferId(): TransferId = transferId
}

class PredefinedIdGeneratorService(accountIds: Seq[AccountId],
                                   transferIds: Seq[TransferId]) extends IdGeneratorService {

  var accountIdIdx = 0
  var transferIdIdx = 0

  override def generateAccountId(): AccountId = {
    val accountId = accountIds(accountIdIdx)
    accountIdIdx += 1
    accountId
  }
  override def generateTransferId(): TransferId = {
    val transferId = transferIds(transferIdIdx)
    transferIdIdx += 1
    transferId
  }
}