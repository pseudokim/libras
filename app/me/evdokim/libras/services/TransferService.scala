package me.evdokim.libras.services

import javax.inject.Inject

import me.evdokim.libras.models.{Transfer, TransferCreation, TransferId, TransferStatus}
import me.evdokim.libras.repositories.TransferRepository

trait TransferService {
  def create(transferCreation: TransferCreation): Transfer
  def getById(id: TransferId): Option[Transfer]
}

class TransferServiceImpl @Inject() (executionService: TransferExecutionService,
                                     transferRepository: TransferRepository,
                                     idGeneratorService: IdGeneratorService) extends TransferService {

  override def create(transferCreation: TransferCreation): Transfer = {
    val transfer = Transfer(
      id = idGeneratorService.generateTransferId(),
      from = transferCreation.from,
      to = transferCreation.to,
      amount = transferCreation.amount,
      status = TransferStatus.Pending
    )
    transferRepository.create(transfer) match {
      case None => create(transferCreation)
      case Some(_) => executionService.execute(transfer)
    }
  }

  override def getById(id: TransferId): Option[Transfer] = {
    transferRepository.getById(id)
  }
}