package me.evdokim.libras.endpoints

import me.evdokim.libras.models.{Account, AccountCreation, AccountId}
import me.evdokim.libras.repositories.AccountRepository
import me.evdokim.libras.services.{ConstantIdGeneratorService, IdGeneratorService}
import org.scalatestplus.play._
import org.scalatestplus.play.guice._
import play.api.Application
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.libs.json.{JsObject, Json}
import play.api.test.Helpers._
import play.api.test._
import play.api.inject.bind

class AccountEndpointSpec extends PlaySpec with GuiceOneAppPerTest with Injecting {

  val idGenerator: ConstantIdGeneratorService = new ConstantIdGeneratorService()
  def withApplication(f: Application => Any) = {
    val app = new GuiceApplicationBuilder()
      .overrides(bind[IdGeneratorService].toInstance(idGenerator))
      .build()
    f(app)
  }

  "GET /accounts/{id}" should {
    "return 'Not found' if account does not exist" in {
      withApplication { app =>
        val result = route(app, FakeRequest(GET, s"/account/${idGenerator.accountId}")).get
        status(result) mustBe NOT_FOUND
      }
    }

    "return previously created account" in {
      withApplication { app =>
        val accountRepository = app.injector.instanceOf[AccountRepository]
        accountRepository.create(Account(idGenerator.accountId, "John", 9.99))
        val result = route(app, FakeRequest(GET, s"/accounts/${idGenerator.accountId}")).get
        status(result) mustBe OK
        contentAsJson(result) mustBe Json.obj(
          "id" -> idGenerator.accountId.toString,
          "name" -> "John",
          "balance" -> 9.99
        )
      }
    }
  }

  "POST /accounts" should {
    "return 'Bad request' for invalid output" in {
      withApplication { app =>
        val request = FakeRequest(POST, s"/accounts")
          .withJsonBody(Json.obj(
            "invalid" -> "object"
          ))
        val result = route(app, request).get
        status(result) mustBe BAD_REQUEST
      }
    }

    "return created account on success" in {
      withApplication { app =>
        val request = FakeRequest(POST, s"/accounts")
          .withJsonBody(Json.obj(
            "name" -> "Bob",
            "initial_balance" -> 100
          ))

        val result = route(app, request).get
        status(result) mustBe CREATED
        contentAsJson(result) mustBe Json.obj(
          "id" -> idGenerator.accountId.toString,
          "name" -> "Bob",
          "balance" -> 100
        )
      }
    }

  }


}
