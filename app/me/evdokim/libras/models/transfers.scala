package me.evdokim.libras.models

import java.util.UUID

import play.api.libs.json.{JsPath, Reads, Writes}
import play.api.libs.functional.syntax._

import scala.util.Try

object TransferId {
  def random(): TransferId = TransferId(UUID.randomUUID())
  implicit val writes: Writes[TransferId] = Writes.of[String].contramap(_.toString)
  def fromString(s: String): Option[TransferId] = {
    Try(TransferId(UUID.fromString(s))).toOption
  }
}
case class TransferId(value: UUID) extends AnyVal {
  override def toString: String = value.toString
}

object TransferStatus {
  case object Pending extends TransferStatus {
    override val state: String = "pending"
    override val reason_code: Option[String] = None
  }
  case object Completed extends TransferStatus {
    override val state: String = "completed"
    override val reason_code: Option[String] = None
  }
  case class Declined(failure: TransferExecutionError) extends TransferStatus {
    override val state: String = "declined"
    override val reason_code: Option[String] = Some(failure.code)
  }
}
sealed trait TransferStatus {
  val state: String
  val reason_code: Option[String]
}



object TransferCreation {
  implicit val reads: Reads[TransferCreation] = (
    (JsPath \ "from").read[AccountId] and
      (JsPath \ "to").read[AccountId] and
      (JsPath \ "amount").read[Double]
    )(TransferCreation.apply _)
}
case class TransferCreation(from: AccountId, to: AccountId, amount: Double)


object Transfer {
  implicit val writes: Writes[Transfer] = (
    (JsPath \ "id").write[TransferId] and
    (JsPath \ "from").write[AccountId] and
    (JsPath \ "to").write[AccountId] and
    (JsPath \ "amount").write[Double] and
    (JsPath \ "state").write[String] and
    (JsPath \ "reason_code").writeNullable[String]
  )(t => (t.id, t.from, t.to, t.amount, t.status.state, t.status.reason_code))
}

case class Transfer(id: TransferId, from: AccountId, to: AccountId,
                    amount: Double, status: TransferStatus)



