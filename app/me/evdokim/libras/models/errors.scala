package me.evdokim.libras.models

import play.api.http.Status
import play.api.libs.json.{Format, JsError, Json}
import play.api.mvc.{Result, Results}

sealed trait LibrasError {
  val problem: JsonProblem
}
case class InvalidUUID(s: String) extends LibrasError {
  val problem: JsonProblem = JsonProblem.badRequest(s"Invalid UUID format: $s")
}
case class AccountNotFound(accountId: AccountId) extends LibrasError {
  val problem: JsonProblem = JsonProblem.notFound(s"Account $accountId does not exist")
}
case class TransferNotFound(transferId: TransferId) extends LibrasError {
  val problem: JsonProblem = JsonProblem.notFound(s"Transfer $transferId does not exist")
}
case class InvalidJsonFormat(jsError: JsError) extends LibrasError  {
  val problem: JsonProblem = JsonProblem.badRequest(s"Error during parsing the json: ${jsError.errors}")
}

object TransferExecutionError {
  case class AccountDoesNotExists(transfer: Transfer, id: AccountId) extends TransferExecutionError {
    override val code: String = s"Account $id does not exists"
    override val problem: JsonProblem = JsonProblem.badRequest(code)
  }
  case class InsufficientBalance(transfer: Transfer) extends TransferExecutionError {
    override val code: String = s"Insufficient balance on account ${transfer.from}"
    override val problem: JsonProblem = JsonProblem.preconditionFailed(code)
  }
}

sealed trait TransferExecutionError extends LibrasError{
  val code: String
}

object JsonProblem {
  def notFound(detail: String) = JsonProblem("Not Found", Status.NOT_FOUND, detail)
  def badRequest(detail: String) = JsonProblem("Bad Request", Status.BAD_REQUEST, detail)
  def preconditionFailed(detail: String) = JsonProblem("Precondition Failed", Status.PRECONDITION_FAILED, detail)
  implicit val format: Format[JsonProblem] = Json.format[JsonProblem]
}

case class JsonProblem(title: String, status: Int, detail: String) {
  def result: Result = {
    new Results.Status(status).apply(Json.toJson(this))
  }
}