# Libras

In-memory transfer service.

## Instructions

* `sbt run` – runs the server
* `sbt test` – executes tests
* Once server is started, Swagger UI will be available on the following address: http://localhost:9000/docs/swagger-ui/index.html?url=/assets/swagger.json

## Implementation notes

* The service is written in Scala using Play Framework
* The implementation is completely non-blocking instead compare and set operations are widely used
* The interface of repositories made intentionally simple so that a persistent version can be easily provided backed by any key-value database that supports compare and set operations (e.g. Cassandra, DynamoDB).
