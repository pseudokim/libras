package me.evdokim.libras.modules

import com.google.inject.AbstractModule
import me.evdokim.libras.repositories.{AccountRepository, InMemoryAccountRepository, InMemoryTransferRepository, TransferRepository}
import me.evdokim.libras.services._

class LibraModule extends AbstractModule {
  override def configure() = {
    bind(classOf[AccountService]).to(classOf[AccountServiceImpl]).asEagerSingleton()
    bind(classOf[TransferService]).to(classOf[TransferServiceImpl]).asEagerSingleton()
    bind(classOf[TransferExecutionService]).to(classOf[TransferExecutionServiceImpl]).asEagerSingleton()
    bind(classOf[IdGeneratorService]).to(classOf[RandomIdGeneratorService]).asEagerSingleton()

    bind(classOf[AccountRepository]).to(classOf[InMemoryAccountRepository]).asEagerSingleton()
    bind(classOf[TransferRepository]).to(classOf[InMemoryTransferRepository]).asEagerSingleton()
  }
}
