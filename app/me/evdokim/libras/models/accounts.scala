package me.evdokim.libras.models

import java.util.UUID

import play.api.libs.functional.syntax._
import play.api.libs.json._
import play.api.libs.json.Writes._

import scala.util.Try

object AccountId {
  def random(): AccountId = AccountId(UUID.randomUUID())
  def fromString(s: String): Option[AccountId] = {
    Try(AccountId(UUID.fromString(s))).toOption
  }
  implicit val writes: Writes[AccountId] = Writes.of[String].contramap(_.toString)
  implicit val reads: Reads[AccountId] = Reads.of[String].map(s => AccountId(UUID.fromString(s)))
}
case class AccountId(value: UUID) extends AnyVal {
  override def toString: String = value.toString
}

object Account {
  implicit val writes: Writes[Account] = (
    (JsPath \ "id").write[AccountId] and
    (JsPath \ "name").write[String] and
    (JsPath \ "balance").write[Double]
  )(unlift(Account.unapply))
}
case class Account(id: AccountId, name: String, balance: Double)

object AccountCreation {
  implicit val reads: Reads[AccountCreation] = (
    (JsPath \ "name").read[String] and
    (JsPath \ "initial_balance").read[Double]
  )(AccountCreation.apply _)
}
case class AccountCreation(name: String, initialBalance: Double)