package me.evdokim.libras.services

import javax.inject.Inject

import me.evdokim.libras.models._
import me.evdokim.libras.repositories.AccountRepository
import play.api.Logger

trait AccountService {
  def create(accountCreation: AccountCreation): Account
  def getById(id: AccountId): Option[Account]
}

class AccountServiceImpl @Inject() (accountRepository: AccountRepository,
                                    idGeneratorService: IdGeneratorService) extends AccountService {
  override def create(accountCreation: AccountCreation): Account = {
    val accountId = idGeneratorService.generateAccountId()
    val account = Account(
      id = accountId,
      name = accountCreation.name,
      balance = accountCreation.initialBalance
    )
    accountRepository.create(account) match {
      case Some(acc) => acc
      case None => {
        Logger.warn(s"Account with id=$accountId is already exists, retrying")
        create(accountCreation)
      }
    }
  }

  override def getById(id: AccountId): Option[Account] = {
    accountRepository.getById(id)
  }
}