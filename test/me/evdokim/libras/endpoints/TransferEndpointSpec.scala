package me.evdokim.libras.endpoints

import me.evdokim.libras.models._
import me.evdokim.libras.repositories.TransferRepository
import me.evdokim.libras.services.{ConstantIdGeneratorService, IdGeneratorService, PredefinedIdGeneratorService}
import org.scalatestplus.play._
import org.scalatestplus.play.guice._
import play.api.Application
import play.api.inject.bind
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.libs.json.{JsNumber, JsString, Json}
import play.api.test.Helpers._
import play.api.test._

class TransferEndpointSpec extends PlaySpec with GuiceOneAppPerTest with Injecting {

  def withApplication[T](accountIds: Seq[AccountId] = Seq.empty, transferIds: Seq[TransferId] = Seq.empty)
                     (f: Application => T): T = {
    val idGeneratorService = new PredefinedIdGeneratorService(accountIds, transferIds)
    val app = new GuiceApplicationBuilder()
      .overrides(bind[IdGeneratorService].toInstance(idGeneratorService))
      .build()
    f(app)
  }

  def createAccount(app: Application, accountCreation: AccountCreation): Unit = {
    val createAccountRequest = FakeRequest(POST, s"/accounts")
      .withJsonBody(Json.obj(
        "name" -> accountCreation.name,
        "initial_balance" -> accountCreation.initialBalance
      ))

    status(route(app, createAccountRequest).get) mustBe CREATED
  }

  "GET /transfers/{id}" should {
    "return 'Not found' if transfer does not exist" in {
      withApplication() { app =>
        val result = route(app, FakeRequest(GET, s"/transfers/${TransferId.random()}")).get
        status(result) mustBe NOT_FOUND
      }
    }

    "return previously created completed transfer" in {
      val transferId = TransferId.random()
      withApplication(transferIds = Seq(transferId)) { app =>
        val transferRepository = app.injector.instanceOf[TransferRepository]
        val transfer = Transfer(
          id = transferId,
          from = AccountId.random(),
          to = AccountId.random(),
          amount = 10,
          status = TransferStatus.Completed
        )
        transferRepository.create(transfer)
        val result = route(app, FakeRequest(GET, s"/transfers/$transferId")).get
        status(result) mustBe OK
        contentAsJson(result) mustBe Json.obj(
          "id" -> transfer.id.toString,
          "from" -> transfer.from.toString,
          "to" -> transfer.to.toString,
          "amount" -> 10,
          "state" -> "completed"
        )
      }
    }
  }

  "POST /transfers" should {
    "create and decline transfer if account does not exists" in {
      val transferId = TransferId.random()
      withApplication(transferIds = Seq(transferId)) { app =>
        val request = FakeRequest(POST, s"/transfers")
          .withJsonBody(Json.obj(
            "from" -> AccountId.random(),
            "to" -> AccountId.random(),
            "amount" -> 100
          ))

        val result = route(app, request).get
        status(result) mustBe CREATED
        (contentAsJson(result) \ "state").get mustBe JsString("declined")
      }
    }

    "create and decline transfer if from account has insufficient balance" in {
      val bobAccountId = AccountId.random()
      val johnAccountId = AccountId.random()
      val transferId = TransferId.random()
      withApplication(Seq(bobAccountId, johnAccountId), Seq(transferId)) { app =>
        createAccount(app, AccountCreation("Bob", 10))
        createAccount(app, AccountCreation("John", 50))
        val request = FakeRequest(POST, s"/transfers")
          .withJsonBody(Json.obj(
            "from" -> bobAccountId,
            "to" -> johnAccountId,
            "amount" -> 100
          ))
        val result = route(app, request).get
        status(result) mustBe CREATED
        (contentAsJson(result) \ "state").get mustBe JsString("declined")
      }
    }

    "create and execute transfer if everything is ok" in {
      val bobAccountId = AccountId.random()
      val johnAccountId = AccountId.random()
      val transferId = TransferId.random()
      withApplication(Seq(bobAccountId, johnAccountId), Seq(transferId)) { app =>
        createAccount(app, AccountCreation("Bob", 100))
        createAccount(app, AccountCreation("John", 200))
        val request = FakeRequest(POST, s"/transfers")
          .withJsonBody(Json.obj(
            "from" -> bobAccountId,
            "to" -> johnAccountId,
            "amount" -> 10
          ))

        val result = route(app, request).get
        status(result) mustBe CREATED
        (contentAsJson(result) \ "state").get mustBe JsString("completed")

        val bobAccountResult = route(app, FakeRequest(GET, s"/accounts/$bobAccountId")).get
        (contentAsJson(bobAccountResult) \ "balance").get mustBe JsNumber(90)

        val johnAccountResult = route(app, FakeRequest(GET, s"/accounts/$johnAccountId")).get
        (contentAsJson(johnAccountResult) \ "balance").get mustBe JsNumber(210)
      }
    }
  }


}
