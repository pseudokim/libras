package me.evdokim.libras.controllers

import javax.inject._

import me.evdokim.libras.models._
import me.evdokim.libras.services.AccountService
import play.api.libs.json.Json
import play.api.mvc._

@Singleton
class AccountController @Inject()(cc: ControllerComponents, accountService: AccountService) extends AbstractController(cc) {

  def getById(id: String) = Action { implicit request: Request[AnyContent] =>
    AccountId.fromString(id) match {
      case None => InvalidUUID(id).problem.result
      case Some(accountId) =>
        accountService.getById(accountId) match {
          case None => AccountNotFound(accountId).problem.result
          case Some(account) => Ok(Json.toJson(account))
        }
    }
  }

  def create = Action(parse.json[AccountCreation]) { implicit request: Request[AccountCreation] =>
    val accountCreation = request.body
    val account = accountService.create(accountCreation)
    Created(Json.toJson(account))
  }

}
