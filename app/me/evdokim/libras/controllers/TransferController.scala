package me.evdokim.libras.controllers

import java.util.UUID
import javax.inject._

import me.evdokim.libras.models._
import me.evdokim.libras.services.TransferService
import play.api.libs.json.Json
import play.api.mvc._

@Singleton
class TransferController @Inject()(cc: ControllerComponents, transferService: TransferService) extends AbstractController(cc) {

  def getById(id: String) = Action { implicit request: Request[AnyContent] =>
    val transferId = TransferId(UUID.fromString(id))
    TransferId.fromString(id) match {
      case None => InvalidUUID(id).problem.result
      case Some(accountId) =>
        transferService.getById(transferId) match {
          case None => TransferNotFound(transferId).problem.result
          case Some(transfer) => Ok(Json.toJson(transfer))
        }
    }
  }

  def create = Action(parse.json[TransferCreation]) { implicit request: Request[TransferCreation] =>
    val transferCreation = request.body
    val account = transferService.create(transferCreation)
    Created(Json.toJson(account))
  }

}
