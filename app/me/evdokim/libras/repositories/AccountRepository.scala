package me.evdokim.libras.repositories

import java.util.concurrent.ConcurrentHashMap

import me.evdokim.libras.models.{Account, AccountId}

trait AccountRepository {
  def create(account: Account): Option[Account]
  def compareAndSet(oldValue: Account, newValue: Account): Boolean
  def getById(id: AccountId): Option[Account]
}

class InMemoryAccountRepository extends AccountRepository {
  private val accounts = new ConcurrentHashMap[AccountId, Account]()

  override def create(account: Account): Option[Account] = {
    Option(accounts.putIfAbsent(account.id, account)) match {
      case None => Some(account)
      case Some(existingAccount) => None
    }
  }

  override def compareAndSet(oldValue: Account, newValue: Account): Boolean = {
    accounts.replace(oldValue.id, oldValue, newValue)
  }

  override def getById(id: AccountId): Option[Account] = {
    Option(accounts.get(id))
  }
}
