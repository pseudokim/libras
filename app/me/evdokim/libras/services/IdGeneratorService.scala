package me.evdokim.libras.services

import me.evdokim.libras.models.{AccountId, TransferId}

trait IdGeneratorService {
  def generateAccountId(): AccountId
  def generateTransferId(): TransferId
}

class RandomIdGeneratorService extends IdGeneratorService {
  override def generateAccountId(): AccountId = AccountId.random()
  override def generateTransferId(): TransferId = TransferId.random()
}

