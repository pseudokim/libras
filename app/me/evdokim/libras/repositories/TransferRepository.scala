package me.evdokim.libras.repositories

import java.util.concurrent.ConcurrentHashMap

import me.evdokim.libras.models.{Transfer, TransferId}

trait TransferRepository {
  def getById(id: TransferId): Option[Transfer]
  def create(transfer: Transfer): Option[Transfer]
  def compareAndSet(oldValue: Transfer, newValue: Transfer): Boolean
}

class InMemoryTransferRepository extends TransferRepository {

  private val transfers = new ConcurrentHashMap[TransferId, Transfer]()

  override def getById(id: TransferId): Option[Transfer] = {
    Option(transfers.get(id))
  }

  override def create(transfer: Transfer): Option[Transfer] = {
    Option(transfers.putIfAbsent(transfer.id, transfer)) match {
      case None => Some(transfer)
      case Some(existingTransfer) => None
    }
  }

  override def compareAndSet(oldValue: Transfer, newValue: Transfer): Boolean = {
    transfers.replace(oldValue.id, oldValue, newValue)
  }
}
