name := "libras"
organization := "me.evdokim"

version := "0.1.0"

lazy val root = (project in file(".")).enablePlugins(PlayScala, SwaggerPlugin)

scalaVersion := "2.12.4"

libraryDependencies ++= Seq(
  guice,
  "org.webjars"             % "swagger-ui"          % "2.2.0",
  "org.scalatestplus.play"  %% "scalatestplus-play" % "3.1.2"   % Test
)

swaggerDomainNameSpaces := Seq("me.evdokim.libras.models")